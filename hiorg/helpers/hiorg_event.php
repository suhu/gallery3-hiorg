<?php defined("SYSPATH") or die("No direct script access.");

    
class hiorg_event_Core {

  /**
  * add hiorg login link to menu
  */
  static function user_menu($menu, $theme) {
    $user = identity::active_user();
    if ($user->guest) {
      // add ours
      $menu->append(Menu::factory("link")
                    ->id("hiorg-login")
                    ->css_id("hiorg-login")
                    ->url(url::site("hiorg/login"))
                    ->label(t("Login via HiOrg")));
    }
  }

}
