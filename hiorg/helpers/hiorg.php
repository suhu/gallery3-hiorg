<?php defined("SYSPATH") or die("No direct script access.");

/**
* This is the API for handling hiorg data
*/
class hiorg_Core {
    
    public static function testCall() {
        print "Hello, world!";
    }
    
    /**
     * Implode an array with the key and value pair giving
     * a glue, a separator between pairs and the array
     * to implode.
     * @param string $glue The glue between key and value
     * @param string $separator Separator between pairs
     * @param array $array The array to implode
     * @return string The imploded array
     */
    static function array_implode( $glue, $separator, $array ) {
        if ( ! is_array( $array ) ) return $array;
        $string = array();
        foreach ( $array as $key => $val ) {
            if ( is_array( $val ) )
                $val = implode( ',', $val );
            $string[] = "{$key}{$glue}{$val}";
        }
        return implode( $separator, $string );
    }

    static function process_user($attributes) {
      print("process_user called");
        $debug = true; // module::get_var("hiorg","logDebugInfo",false);
      
    if($debug) {
      Kohana_Log::add("information", 
        sprintf("[HiOrg SSO Module] Processing user login: %s [process_user()]\n",
          hiorg::array_implode(':',',',$attributes)
        )
      );
    }

    $user = NULL;
    $valid = false;

    $user = identity::lookup_user_by_name( $attributes['username'] );
      
    if (is_null($user)) {
        if($debug) {
            Kohana_Log::add("information", "[HiOrg SSO Module] No identity found: provisioning user [process_user()]\n");
        }
        $user = hiorg::create_new_user($attributes);
        $valid = true;
    }
    else {
        // we have an authenticated identity AND we have a user, so log them in!
        if($debug) {
            Kohana_Log::add("information", "[HiOrg SSO Module] Found an identity & user. Will log in '{$user->name}' [process_user()]\n");
        }
        $valid = true;
    }

    if ($valid) {
      // double check that everything is hunky-dorry
      if(is_null($user)) {
        if($debug) {
          Kohana_Log::add("information", "[HiOrg SSO Module] For some reason, user information is now null! [process_user()]\n");
        }
        $valid = false;
      } else {
        if($debug) {
          Kohana_Log::add("information", "[HiOrg SSO Module] logging in the user: {$user->name} [process_user()]\n");
        }
        auth::login($user);
      }
    }

    return $valid;
  }
    
    static function create_new_user( $attributes ) {
        $debug = module::get_var("hiorg","logDebugInfo",false);
        $name = $attributes['username'];

        if($debug) {
            Kohana_Log::add("information", "[HiOrg SSO Module] Trying to create user '{$name}'. [create_new_user]\n");
        }

        $fullName = $attributes['vorname'].' '.$attributes['name'];
        $email = (isset($attributes['email']) ? $attributes['email'] : "");
        $password = md5(uniqid(mt_rand(), true));
        
        $user = identity::create_user($name, $fullName, $password, $email);
        $user->admin = false;
        $user->guest = false;
        $user->save();

        // reload user from database
        return identity::lookup_user_by_name( $name );
    }
}


?>
